(function() {'use strict';
angular.module('maratona')
.controller('HomeCtrl', function($scope, marathonsFactory, Auth) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.marathons = marathonsFactory.marathons;
    // $scope.incrementUpvotes = function(marathon) {
    //     marathons.upvote(marathon);
    // };
})
})();