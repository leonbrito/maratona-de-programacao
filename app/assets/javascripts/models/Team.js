class Team {

    constructor({id, name, score, created_at, updated_at}) {
        this._id = id;
        this._name = name;
        this._score = score;
        this._created_at = created_at;
        this._updated_at = updated_at;
        this._answers = [];
        this._pontuacao = 0;
    }

    get id() {
        return this._id;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }  

    get score() {
        return this._score;
    }

    set score(score) {
        this._score = score;
    }   

    get created_at() {
        return this._created_at;
    }

    get updated_at() {
        return this._updated_at;
    }

    get answers() {
        return this._answers;
    }

    set answers(answers) {
        this._answers = answers;
    }

    get pontuacao() {
        return this._pontuacao;
    }

    set pontuacao(pontos) {
        this._pontuacao = pontos;
    }

    addAnswer(answer) {
        this._answers.push(answer);
        this.pontuacao = this.getPontuacao();
    }

    getPontuacao(time) {
        var qt = (this.slv() * 100000) + (3600 - this.calcTime()) - time;
        return qt;
    }

    slv(){
        var i = 0;
        this.answers.forEach(function(answer){
            if(answer.status == 'certain'){
                i++;
            }
        });
        return i;
    }

    calcTime() {
        var sum = 0;
        this.answers.forEach(function(answer) {
            if(answer.status == 'certain'){
                let hour = 0;
                let minutes = 0;
                let seconds = 0;

                var time = answer.updated_at;
                
                time = time.split('T');
                time = time[1].split('.');
                time = time[0].split(':');
                // console.log(time);

                var times = [];
                for (var i = 0; i < 3; i++) {
                    times[i] = (isNaN(parseInt(time[i]))) ? 0 : parseInt(time[i]);
                }
                hour = times[0];
                minutes = times[1] + answer.attempt * 20;
                seconds = times[2];
                // console.log("hora:"+hour+" minutos:"+minutes+" segundos:"+seconds)
                hour = hour * 60;
                minutes += hour;
                minutes += seconds / 60;

                sum += minutes;
                // console.log("Soma:"+sum);
            }
        });
        return sum;
    }
}