class QuestionsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  # GET /questions.json
  def index
    respond_with Question.all
  end

  # GET /questions/1.json
  def show
     respond_with Question.find(params[:id])
  end

  # GET /questions/new
  # def new
  #   @question = Question.new
  # end

  # # GET /questions/1/edit
  # def edit
  # end

  # POST /questions.json
  def create
    @question = Question.new(question_params)
    if @question.save
      respond_with @question
    else
      respond_with @question.errors
    end
  end

  # PATCH/PUT /questions/1.json
  def update
    @question = Question.find(params[:id])
    @question.name = params[:name]
    if @question.save!
      respond_with @question 
    else
      respond_with @question.errors
    end
  end

  # DELETE /questions/1.json
  def destroy
    respond_with Question.find(params[:id]).destroy
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:name)
    end
end