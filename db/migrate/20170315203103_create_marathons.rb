class CreateMarathons < ActiveRecord::Migration[5.0]
  def change
    create_table :marathons do |t|
      t.time :duration
      t.column :status, :integer, default: 0, null: false

      t.timestamps null: false
    end
  end
end
