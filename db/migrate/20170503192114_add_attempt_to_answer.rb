class AddAttemptToAnswer < ActiveRecord::Migration[5.0]
  def change
    add_column :answers, :attempt, :integer, default: 0, null: false
    add_column :answers, :status, :integer, default: 0, null: false
  end
end
