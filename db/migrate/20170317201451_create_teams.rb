class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.string :name
      t.float :score, default: 0, null: false

      t.timestamps
    end
  end
end
