class MarathonsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_marathon, only: [:show, :edit, :update, :destroy]
  # GET /marathons.json
  def index
    respond_with Marathon.all
    # respond_with Marathon.all
  end

  # GET /marathons/1.json
  def show
    respond_with @marathon
  end

  # GET /marathons/new
  # def new
  #   respond_with = Marathon.new
  # end

  # GET /marathons/1/edit
  # def edit
  # end

  # POST /marathons.json
  def create
    @marathon = Marathon.new(marathon_params.merge(user_id: current_user.id))
    respond_to do |format|
      if @marathon.save
        format.json { respond_with @marathon, status: :created, location: @marathon }
      else
        format.json { respond_with json: @marathon.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /marathons/1.json
  def update
    respond_to do |format|
      if @marathon.update(marathon_params)
        format.json { respond_with :show, status: :ok, location: @marathon }
      else
        format.json { respond_with json: @marathon.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /marathons/1.json
  def destroy
    @marathon.destroy
    respond_to do |format|
      format.html { redirect_to marathons_url, notice: 'Marathon was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_marathon
      @marathon = Marathon.find(params[:id])
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def marathon_params
      params.require(:marathon).permit(:duration, :status)
    end
end