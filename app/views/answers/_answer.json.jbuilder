json.extract! answer, :id, :marathon_id, :question_id, :team_id, :created_at, :updated_at, :attempt, :status
json.url answer_url(answer, format: :json)
