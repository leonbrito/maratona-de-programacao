(function() {'use strict';
angular.module('maratona')
.controller('TeamsShowCtrl', function($scope, Auth, $mdDialog, team, teamsFactory) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.team = team;
    var assoc_team = null;
    var assoc_member = null;
    console.log($scope.team);

    // ******************************
    // Dialog MemberOfTeams
    // ******************************

    $scope.openDialogMember = function($event) {
      $mdDialog.show({
        controller: DialogMemberCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'assets/team/_assocMemberOfTeamsDialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true,
        resolve: {
            memberOfTeamsPromise: function(memberOfTeamsFactory) {
                return memberOfTeamsFactory.getAll();
            }
        }
      })
    }

  function DialogMemberCtrl ($timeout, $q, $mdDialog, teamsFactory, memberOfTeamsFactory) {
    var self = this;
    self.simulateQuery = false;
    self.isDisabled    = false;

    self.members = loadAll();
    self.querySearch   = querySearch;
    self.addMember = addMember;

    // ******************************
    // Template methods
    // ******************************

    self.cancel = function($event) {
      $mdDialog.cancel();
    };
    self.finish = function($event) {
      $mdDialog.hide();
    };

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
      var results = query ? self.members.filter( createFilterFor(query) ) : self.members,
          deferred;

      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }

    function addMember() {
      if(!self.selectedItem || self.selectedItem === ''){ return; }
      teamsFactory.addMember({
        member_of_team_id: self.selectedItem.id,
        team_id: team.id
      });
      team.member_of_teams.push(self.selectedItem);
      self.selectedItem = '';
    }

    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
      var repos = memberOfTeamsFactory.memberOfTeams;
      return repos.map( function (repo) {
        repo.value = repo.name.toLowerCase();
        return repo;
      });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(item) {
        return (item.value.indexOf(lowercaseQuery) === 0);
      };

    }
  }

  $scope.openDialogRemoveMember = function($event, team, member) {
      assoc_team = team;
      assoc_member = member;
      $mdDialog.show({
        controller: DialogRemoveMemberCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'assets/team/_removeMemberDialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true
      })
    }
    
    function DialogRemoveMemberCtrl ($timeout, $q, $scope, $mdDialog) {
      var self = this;

      // ******************************
      // Template methods
      // ******************************

      self.removeMember = function($event) {
        console.log(assoc_team, assoc_member);
        teamsFactory.removeMember({team_id: assoc_team.id, member_of_team_id: assoc_member.id});
        team.member_of_teams.pop(assoc_member);
        $mdDialog.cancel();
      };
      self.cancel = function($event) {
        $mdDialog.cancel();
      };
      self.finish = function($event) {
        $mdDialog.hide();
      };
    }

})
})();