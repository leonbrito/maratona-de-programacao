(function() {'use strict';
angular.module('maratona')
.controller('QuestionsEditCtrl', function($scope, questionsFactory, question, Auth) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.question = question;

    $scope.editQuestion = function() {
        if(!$scope.question.name || $scope.question.name === ''){ return; }
        console.log($scope.question);
        questionsFactory.update(question);
    };
})
})();