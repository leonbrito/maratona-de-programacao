(function() {'use strict';
angular.module('maratona')
.factory('teamsFactory', function($http, $state, UrlConstant) {

    var o = {
        teams: []
    };

    o.getAll = function() {
        return $http.get(UrlConstant.teams).then(function(res){
            angular.copy(res.data, o.teams);
        });
    };

    o.get = function(id) {
        return $http.get(UrlConstant.team + id + '.json').then(function(res) {
            return res.data;
        });
    };

    o.create = function(team) {
        return $http.post(UrlConstant.teams, team).then(function(data) {
            o.teams.push(data.data);
        });
    };

    o.update = function(team) {
        return $http.put('/teams/' + team.id + '.json', team).then(function(data) {
            $state.go('teams');
        });
    };
    
    o.delete = function(team) {
        return $http.delete('/teams/' + team.id + '.json', team).then(function(res){
            for (var i = 0; i < o.teams.length; i++) {
                if(o.teams[i].id == team.id){
                    o.teams.splice( i, 1 )
                    break;
                }
            }
        });
    };

    o.addMember = function(memberTeam) {
        return $http.post('/team_member_of_teams', memberTeam).then(function(data) {
        });
    };

    o.removeMember = function(memberTeam) {
        return $http.delete('/remove_members?team_id=' 
            + memberTeam.team_id + '&member_of_team_id=' + memberTeam.member_of_team_id , 
            memberTeam).then(function(data) {
        });
    };

    return o;
})
})();