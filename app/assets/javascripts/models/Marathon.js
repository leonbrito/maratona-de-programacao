class Marathon {

    constructor({id, duration, status, created_at, updated_at, user_id, teams, questions, answers}) {
        this._id = id;
        this._duration = duration;
        this._status = status.toUpperCase();
        this._created_at = created_at;
        this._updated_at = updated_at;
        this._user_id = user_id;
        this._teams = [];
        this._questions = [];
        this._timeInicio = this.calcTime(updated_at);
        this._timeFinal = this.calcTime(duration);
        
        for(var i = 0; i < teams.length; i++) {
            this._teams.push(new Team(teams[i]));
        }

        for(var i = 0; i < questions.length; i++) {
            this._questions.push(new Question(questions[i]));
        }

        this._setAnswers(answers);
        this.setPontuacao();
    }

    get id() {
        return this._id;
    }

    get duration() {
        return this._duration;
    }

    set duration(duration) {
        this._duration = duration;
    }

    get status() {
        return this._status;
    }
    
    set status(status) {
        this._status = status;
    }

    get created_at() {
        return this._created_at;
    }

    get updated_at() {
        return this._updated_at;
    }

    get timeInicio(){
        return this._timeInicio;
    }

    get timeFinal(){
        return this._timeFinal + this._timeInicio;
    }

    calcTime(t) {
        let sum = 0;
        let hour = 0;
        let minutes = 0;
        let seconds = 0;
        var time = t;
        time = time.split('T');
        time = time[1].split('.');
        time = time[0].split(':');

        var times = [];
        for (var i = 0; i < 3; i++) {
            times[i] = (isNaN(parseInt(time[i]))) ? 0 : parseInt(time[i]);
        }
        hour = times[0];
        minutes = times[1];
        seconds = times[2];

        hour = hour * 60;
        minutes += hour;
        minutes += seconds / 60;

        return sum += minutes;
    }

    get teams() {
        return this._teams;
    }

    set teams(teams) {
        this._teams = teams;
    }

    get questions() {
        return this._questions;
    }

    set questions(questions) {
        this._questions = questions;
    }

    get duracaoTotal() {
        return this.timeFinal - this.timeInicio;
    }

    setPontuacao() {
        for(var i = 0; i < this.teams.length; i++) {
            this.teams[i].pontuacao = this.teams[i].getPontuacao(this._timeInicio);
        }
    }

    _setAnswers (answers) {
        for(var i = 0; i < answers.length; i++) {
            let answer = new Answer(answers[i]);
            
            for(var j = 0; j < this.teams.length; j++) {
                if(this.teams[j].id == answer.team_id){
                    this.teams[j].addAnswer(answer);
                }
            }
        }
    }

    findAnswer(question, team) {
        var i = 0;
        while(this.teams[i].id != team.id){
            i++;
        }

        for(var j = 0; j < this.teams[i].answers.length; j++) {
            if(this.teams[i].answers[j].question_id == question.id) {
                return this.teams[i].answers[j];
            }
        }
        return null;
    }

    prepareForSend(answer) {
        return answer.prepareForSend(answer);
    }

    teamOrder() {
        this.setPontuacao();

        for (var i = 1; i < this.teams.length; i++){
			var key = this.teams[i];
			var j = i;
			while ((j > 0) && (this.teams[j-1].pontuacao > key.pontuacao)) {
                this.teams[j] = this.teams[j-1];
				j -= 1;
			}
			this.teams[j] = key;
		}
        return this.teams;
    }

    updateTime() {
        let date = new Date().toTimeString();
        let timeAtual = this.calcJavaScriptTime(date);
        let duracaototal = this.duracaoTotal;
        console.log(timeAtual - this.timeInicio);
        return ((timeAtual - this.timeInicio) / duracaototal) * 100;
    }

    calcJavaScriptTime(t) {
        let time = t.split(' ');
        time = time[0].split(':');
        let times = [];
        for (let i = 0; i < 3; i++) {
            times[i] = (isNaN(parseInt(time[i]))) ? 0 : parseInt(time[i]);
        }
        let sum = 0;
        let hour = times[0] * 60;
        let minutes = times[1];
        let seconds = times[2] / 60;
        minutes += hour;
        minutes += seconds;

        return sum += minutes;
    }
}