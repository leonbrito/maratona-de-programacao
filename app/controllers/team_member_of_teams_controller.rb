class TeamMemberOfTeamsController < ApplicationController
  before_action :set_team_member_of_team, only: [:show, :edit, :update, :destroy]

  # GET /team_member_of_teams
  # GET /team_member_of_teams.json
  def index
    @team_member_of_teams = TeamMemberOfTeam.all
  end

  # GET /team_member_of_teams/1
  # GET /team_member_of_teams/1.json
  def show
  end

  # GET /team_member_of_teams/new
  def new
    @team_member_of_team = TeamMemberOfTeam.new
  end

  # GET /team_member_of_teams/1/edit
  def edit
  end

  # POST /team_member_of_teams
  # POST /team_member_of_teams.json
  def create
    @team_member_of_team = TeamMemberOfTeam.new(team_member_of_team_params)

    respond_to do |format|
      if @team_member_of_team.save
        format.html { redirect_to @team_member_of_team, notice: 'Team member of team was successfully created.' }
        format.json { render :show, status: :created, location: @team_member_of_team }
      else
        format.html { render :new }
        format.json { render json: @team_member_of_team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /team_member_of_teams/1
  # PATCH/PUT /team_member_of_teams/1.json
  def update
    respond_to do |format|
      if @team_member_of_team.update(team_member_of_team_params)
        format.html { redirect_to @team_member_of_team, notice: 'Team member of team was successfully updated.' }
        format.json { render :show, status: :ok, location: @team_member_of_team }
      else
        format.html { render :edit }
        format.json { render json: @team_member_of_team.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def removeMembers
    @team_member_of_team = TeamMemberOfTeam.find_by(team_id: params[:team_id], member_of_team_id: params[:member_of_team_id])
    @team_member_of_team.destroy
  end

  # DELETE /team_member_of_teams/1
  # DELETE /team_member_of_teams/1.json
  def destroy
    @team_member_of_team.destroy
    respond_to do |format|
      format.html { redirect_to team_member_of_teams_url, notice: 'Team member of team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team_member_of_team
      @team_member_of_team = TeamMemberOfTeam.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_member_of_team_params
      params.require(:team_member_of_team).permit(:team_id, :member_of_team_id)
    end
end
