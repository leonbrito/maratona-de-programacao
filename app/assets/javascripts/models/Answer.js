class Answer {
    
    constructor({id, marathon_id, question_id, team_id, created_at, updated_at, attempt, status}) {
        this._id = id;
        this._marathon_id = marathon_id;
        this._question_id = question_id;
        this._team_id = team_id;
        this._created_at = created_at;
        this._updated_at = updated_at;
        this._attempt = attempt;
        this._status = status;
    }

    get id() {
        return this._id;
    }

    get marathon_id() {
        return this._marathon_id;
    } 

    get question_id() {
        return this._question_id;
    }

    get team_id() {
        return this._team_id;
    }

    get attempt() {
        return this._attempt;
    }

    set attempt(attempt) {
        this._attempt = attempt;
    }

    get status() {
        return this._status;
    }

    set status(status) {
        this._status = status;
    }

    get created_at() {
        return this._created_at;
    }

    get updated_at() {
        return this._updated_at;
    }

    prepareForSend(answer) {
        return {
            id: answer.id,
            marathon_id: answer.marathon_id,
            question_id: answer.question_id,
            team_id: answer.team_id,
            created_at: answer.created_at,
            updated_at: answer.updated_at,
            attempt: answer.attempt,
            status: answer.status
        }
    }
}