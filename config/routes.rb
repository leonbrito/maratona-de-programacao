Rails.application.routes.draw do
  resources :answers
  resources :team_member_of_teams
  resources :member_of_teams, only: [:index, :show, :create, :update, :destroy]
  resources :marathon_questions
  resources :questions, only: [:index, :show, :create, :update, :destroy]
  resources :marathon_teams
  resources :teams, only: [:index, :show, :create, :update, :destroy]
  resources :marathons, only: [:index, :show, :create, :update, :destroy]
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  match 'remove_questions' => 'marathon_questions#removeQuestions', via: :delete
  match 'remove_teams' => 'marathon_teams#removeTeams', via: :delete
  match 'remove_members' => 'team_member_of_teams#removeMembers', via: :delete
  root to: 'application#angular'
end