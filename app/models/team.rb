class Team < ApplicationRecord
    has_many :marathon_team, dependent: :destroy
    has_many :marathon, through: :marathon_team
    has_many :team_member_of_team, dependent: :destroy
    has_many :member_of_teams, through: :team_member_of_team
    has_many :answer, dependent: :destroy
    has_many :answers, through: :answer, :source => :team

    def as_json(options = {})
        super(options.merge(include: :member_of_teams))
    end
end