require 'test_helper'

class MemberOfTeamsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @member_of_team = member_of_teams(:one)
  end

  test "should get index" do
    get member_of_teams_url
    assert_response :success
  end

  test "should get new" do
    get new_member_of_team_url
    assert_response :success
  end

  test "should create member_of_team" do
    assert_difference('MemberOfTeam.count') do
      post member_of_teams_url, params: { member_of_team: { name: @member_of_team.name } }
    end

    assert_redirected_to member_of_team_url(MemberOfTeam.last)
  end

  test "should show member_of_team" do
    get member_of_team_url(@member_of_team)
    assert_response :success
  end

  test "should get edit" do
    get edit_member_of_team_url(@member_of_team)
    assert_response :success
  end

  test "should update member_of_team" do
    patch member_of_team_url(@member_of_team), params: { member_of_team: { name: @member_of_team.name } }
    assert_redirected_to member_of_team_url(@member_of_team)
  end

  test "should destroy member_of_team" do
    assert_difference('MemberOfTeam.count', -1) do
      delete member_of_team_url(@member_of_team)
    end

    assert_redirected_to member_of_teams_url
  end
end
