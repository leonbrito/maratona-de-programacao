class Marathon < ApplicationRecord
    enum status: {aberta: 0, iniciada: 1, finalizada: 2}
    belongs_to :user
    has_many :marathon_team, dependent: :destroy
    has_many :teams, through: :marathon_team
    has_many :marathon_question, dependent: :destroy
    has_many :questions, through: :marathon_question
    has_many :answers, :class_name => "Answer", :foreign_key => "marathon_id"

    def as_json(options = {})
        super(options.merge(include: [:user, :teams, :questions, :answers]))
    end
end
