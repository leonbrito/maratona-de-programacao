(function() {'use strict';
angular.module('maratona')
.controller('TeamsCtrl', function($scope, teamsFactory, Auth, $mdDialog, $mdToast) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.teams = teamsFactory.teams;
    var teamDel = null;

    $scope.addTeam = function() {
        if(!$scope.name || $scope.name === ''){ return; }
        console.log($scope.name);
        teamsFactory.create({
            name: $scope.name,
        });
        $scope.name = '';
        $scope.showSimpleToast();
    };

    $scope.showSimpleToast = function() {

      $mdToast.show(
        $mdToast.simple()
          .textContent('Sucesso!')
          .position('top right')
          .hideDelay(3000)
          .theme('success-toast')
      );
    };

    $scope.openDialog = function($event, team) {
      teamDel = team;
      $mdDialog.show({
        controller: DialogCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'assets/team/_deleteDialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true
      })
    }

    function DialogCtrl ($timeout, $q, $scope, $mdDialog) {
      var self = this;
      console.log(teamDel);

      // ******************************
      // Template methods
      // ******************************

      self.delete = function($event) {
        teamsFactory.delete(teamDel);
        $mdDialog.cancel();
      };
      self.cancel = function($event) {
        $mdDialog.cancel();
      };
      self.finish = function($event) {
        $mdDialog.hide();
      };
    }
})
})();