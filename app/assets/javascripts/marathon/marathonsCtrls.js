(function() {'use strict';
angular.module('maratona')
.controller('MarathonsCtrl', function($scope, $mdDialog, marathons, marathonsFactory, Auth, $mdToast) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.marathons = marathonsFactory.marathons;
    $scope.duration = new Date("");
    var marathonDel = null;

    $scope.addMarathon = function() {
        if(!$scope.duration || $scope.duration === ''){ return; }
        console.log($scope.duration);
        marathonsFactory.create({
            duration: new Date($scope.duration).toString()
        });
        $scope.duration = '';
        $scope.showSimpleToast();
    };

    $scope.showSimpleToast = function() {

      $mdToast.show(
        $mdToast.simple()
          .textContent('Sucesso!')
          .position('top right')
          .hideDelay(3000)
          .theme('success-toast')
      );
    };

    $scope.openDialog = function($event, marathon) {
      marathonDel = marathon;
      $mdDialog.show({
        controller: DialogCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'assets/marathon/_deleteDialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true
      })
    }

    function DialogCtrl ($timeout, $q, $scope, $mdDialog) {
      var self = this;

      // ******************************
      // Template methods
      // ******************************

      self.delete = function($event) {
        marathonsFactory.delete(marathonDel);
        $mdDialog.cancel();
      };
      self.cancel = function($event) {
        $mdDialog.cancel();
      };
      self.finish = function($event) {
        $mdDialog.hide();
      };
    }
})
})();