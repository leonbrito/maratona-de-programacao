class Question < ApplicationRecord
    has_many :marathon_question, dependent: :destroy
    has_many :matahon, through: :marathon_question
    has_many :answer, dependent: :destroy
    has_many :answers, through: :answer, :source => :question
end
