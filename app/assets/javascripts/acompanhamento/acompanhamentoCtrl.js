(function() {'use strict';
angular.module('maratona')
.controller('AcompanhamentoCtrl', function($scope, marathon, acompanhamentoFactory, $interval, $mdDialog) {
    var subMarathon;
    var subQuestion;
    var subTeam;
    var maratona = new Marathon(marathon);
    var self = this;

    $scope.marathon = maratona;
    $scope.marathon.teams = maratona.teamOrder();
    $scope.answer;
    $scope.determinateValue = maratona.updateTime();

    $interval(function() {
      if ($scope.marathon.status != 'FINALIZADA') {
          $scope.determinateValue = maratona.updateTime();
      }
      
      if ($scope.determinateValue > 100 && $scope.marathon.status != 'FINALIZADA') {
          alert("A maratona terminou!");
          $scope.marathon.status = 'FINALIZADA';
      }
    }, 5000, 0, true);
    
    $scope.findAnswer = function(question, team) {
        $scope.answer = maratona.findAnswer(question, team);
        if ($scope.answer != null){
         return true;
        }
        return false;
    }

    $scope.openDialogSubmeter = function($event, marathon, team, question, acompanhamentoFactory) {
        subMarathon = marathon;
        subQuestion = question;
        subTeam = team;
        $mdDialog.show({
            controller: DialogSubmeterCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'assets/acompanhamento/_submeterDialog.html',
            parent: angular.element(document.body),
            targetEvent: $event,
            clickOutsideToClose:true
        })
    }
    
    function DialogSubmeterCtrl ($timeout, $q, $scope, $mdDialog, $state, acompanhamentoFactory) {
      var self = this;

      self.marathon = subMarathon;
      self.team = subTeam;
      self.question = subQuestion;

      // ******************************
      // Template methods
      // ******************************

      self.submeter = function($event, status) {
          var sub = maratona.findAnswer(self.question, self.team);
          if(sub != null){
              sub.attemp = sub.attempt++;
              sub.status = status;
              var ans = maratona.prepareForSend(sub);
              acompanhamentoFactory.submeterUpdate(ans).then(function(res){
                  self.marathon.teams = maratona.teamOrder();
              });
          } else {
              acompanhamentoFactory.submeterCreate(newAnswer(subMarathon, subTeam, status)).then(promise);
          }
          
          $mdDialog.cancel(); 
      };

      self.cancel = function($event) {
        $mdDialog.cancel();
      };
      self.finish = function($event) {
        $mdDialog.hide();
      };

      function newAnswer(subMarathon, subTeam, status, attempt) {
        return {marathon_id: subMarathon.id, 
                question_id: subQuestion.id, 
                team_id: subTeam.id, 
                status: status, 
                attempt: "1"
            }
      }

      function promise(data) {
          var answer = new Answer(data);
          self.team.addAnswer(answer);
          self.marathon.teams = maratona.teamOrder();
      }
    }
})
})();