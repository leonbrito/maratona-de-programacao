class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_sign_up_params, if: :devise_controller?
  respond_to :json

  rescue_from ActionController::InvalidAuthenticityToken do |exception|
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
    render text: 'Invalid authenticity token', status: :unprocessable_entity
  end

  def angular
    render 'layouts/application'
  end

  protected

  def configure_sign_up_params
      devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
  end

end
