(function() {'use strict';
angular.module('maratona')
.controller('MemberOfTeamsCtrl', function($scope, memberOfTeamsFactory, Auth, $mdDialog, $mdToast) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.memberOfTeams = memberOfTeamsFactory.memberOfTeams;
    var memberOfTeamDel = null;

    $scope.addMemberOfTeam = function() {
        if(!$scope.name || $scope.name === ''){ return; }
        console.log($scope.name);
        memberOfTeamsFactory.create({
            name: $scope.name,
        });
        $scope.name = null;
        $scope.showSimpleToast();
    };

    $scope.showSimpleToast = function() {

      $mdToast.show(
        $mdToast.simple()
          .textContent('Sucesso!')
          .position('top right')
          .hideDelay(3000)
          .theme('success-toast')
      );
    };

    $scope.openDialog = function($event, memberOfTeam) {
      memberOfTeamDel = memberOfTeam;
      $mdDialog.show({
        controller: DialogCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'assets/member_of_team/_deleteDialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true
      })
    }

    function DialogCtrl ($timeout, $q, $scope, $mdDialog) {
      var self = this;
      console.log(memberOfTeamDel);

      // ******************************
      // Template methods
      // ******************************

      self.delete = function($event) {
        memberOfTeamsFactory.delete(memberOfTeamDel);
        $mdDialog.cancel();
      };
      self.cancel = function($event) {
        $mdDialog.cancel();
      };
      self.finish = function($event) {
        $mdDialog.hide();
      };
    }
})
})();