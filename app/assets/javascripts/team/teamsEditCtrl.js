(function() {'use strict';
angular.module('maratona')
.controller('TeamsEditCtrl', function($scope, team, teamsFactory, Auth) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.team = team;
    console.log($scope.team);
    $scope.editTeam = function(team) {
        if(!$scope.team.name || $scope.team.name === ''){ return; }
        console.log($scope.team);
        teamsFactory.update($scope.team);
    };
})
})();