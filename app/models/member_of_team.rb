class MemberOfTeam < ApplicationRecord
    has_many :team_member_of_team, dependent: :destroy
    has_many :teams, through: :team_member_of_team
end
