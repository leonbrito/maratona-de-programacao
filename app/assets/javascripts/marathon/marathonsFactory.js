(function() {'use strict';
angular.module('maratona')
.factory('marathonsFactory', function($http, $state) {

    var o = {
        marathons: []
    };

    o.getAll = function() {
        return $http.get('/marathons.json').then(function(res) {
            angular.copy(res.data, o.marathons);
        });
    };

    o.get = function(id) {
        return $http.get('/marathons/' + id + '.json').then(function(res) {
            return res.data;
        });
    };

    o.create = function(marathon) {
        return $http.post('/marathons.json', marathon).then(function(data) {
            o.marathons.push(data.data);
        });
    };

    o.update = function(marathon) {
        return $http.put('/marathons/' + marathon.id + '.json', marathon).then(function(data) {
            $state.go('marathons');
        });
    };

    o.iniciarMarathon = function(marathon) {
        marathon.status = "iniciada";
        return $http.put('/marathons/' + marathon.id + '.json', marathon).then(function(data) {
        });
    };

    o.delete = function(marathon) {
        return $http.delete('/marathons/' + marathon.id + '.json', marathon).then(function(res){
            for (var i = 0; i < o.marathons.length; i++) {
                if(o.marathons[i].id == marathon.id){
                    o.marathons.splice( i, 1 )
                    break;
                }
            }
        });
    };

    o.addTeam = function(marathonTeam) {
        return $http.post('/marathon_teams' , marathonTeam).then(function(data) {
        });
    };

    o.removeTeam = function(marathonTeam) {
        return $http.delete('/remove_teams?marathon_id=' 
            + marathonTeam.marathon_id + '&team_id=' + marathonTeam.team_id , marathonTeam).then(function(data) {
        });
    };

    o.addQuestion = function(marathonQuestion) {
        return $http.post('/marathon_questions' , marathonQuestion).then(function(data) {
        });
    };

    o.removeQuestion = function(marathonQuestion) {
        return $http.delete('/remove_questions?marathon_id=' 
            + marathonQuestion.marathon_id + '&question_id=' + marathonQuestion.question_id , 
            marathonQuestion).then(function(data) {
        });
    };

    return o;
})
})();