class MarathonQuestionsController < ApplicationController
  before_action :set_marathon_question, only: [:show, :edit, :update, :destroy]

  # GET /marathon_questions
  # GET /marathon_questions.json
  def index
    @marathon_questions = MarathonQuestion.all
  end

  # GET /marathon_questions/1
  # GET /marathon_questions/1.json
  def show
  end

  # GET /marathon_questions/new
  def new
    @marathon_question = MarathonQuestion.new
  end

  # GET /marathon_questions/1/edit
  def edit
  end

  # POST /marathon_questions
  # POST /marathon_questions.json
  def create
    @marathon_question = MarathonQuestion.new(marathon_question_params)

    respond_to do |format|
      if @marathon_question.save
        format.html { redirect_to @marathon_question, notice: 'Marathon question was successfully created.' }
        format.json { render :show, status: :created, location: @marathon_question }
      else
        format.html { render :new }
        format.json { render json: @marathon_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /marathon_questions/1
  # PATCH/PUT /marathon_questions/1.json
  def update
    respond_to do |format|
      if @marathon_question.update(marathon_question_params)
        format.html { redirect_to @marathon_question, notice: 'Marathon question was successfully updated.' }
        format.json { render :show, status: :ok, location: @marathon_question }
      else
        format.html { render :edit }
        format.json { render json: @marathon_question.errors, status: :unprocessable_entity }
      end
    end
  end

  def removeQuestions
    @marathon_question = MarathonQuestion.find_by(marathon_id: params[:marathon_id], question_id: params[:question_id])
    @marathon_question.destroy
  end

  # DELETE /marathon_questions/1
  # DELETE /marathon_questions/1.json
  def destroy
    @marathon_question.destroy
    respond_to do |format|
      format.html { redirect_to marathon_questions_url, notice: 'Marathon question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_marathon_question
      @marathon_question = MarathonQuestion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def marathon_question_params
      params.require(:marathon_question).permit(:marathon_id, :question_id)
    end
end