# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170516174851) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: :cascade do |t|
    t.integer  "marathon_id"
    t.integer  "question_id"
    t.integer  "team_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "attempt",     default: 0, null: false
    t.integer  "status",      default: 0, null: false
    t.index ["marathon_id"], name: "index_answers_on_marathon_id", using: :btree
    t.index ["question_id"], name: "index_answers_on_question_id", using: :btree
    t.index ["team_id"], name: "index_answers_on_team_id", using: :btree
  end

  create_table "marathon_questions", force: :cascade do |t|
    t.integer  "marathon_id"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["marathon_id"], name: "index_marathon_questions_on_marathon_id", using: :btree
    t.index ["question_id"], name: "index_marathon_questions_on_question_id", using: :btree
  end

  create_table "marathon_teams", force: :cascade do |t|
    t.integer  "marathon_id"
    t.integer  "team_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["marathon_id"], name: "index_marathon_teams_on_marathon_id", using: :btree
    t.index ["team_id"], name: "index_marathon_teams_on_team_id", using: :btree
  end

  create_table "marathons", force: :cascade do |t|
    t.time     "duration"
    t.integer  "status",     default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_marathons_on_user_id", using: :btree
  end

  create_table "member_of_teams", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "questions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "team_member_of_teams", force: :cascade do |t|
    t.integer  "team_id"
    t.integer  "member_of_team_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["member_of_team_id"], name: "index_team_member_of_teams_on_member_of_team_id", using: :btree
    t.index ["team_id"], name: "index_team_member_of_teams_on_team_id", using: :btree
  end

  create_table "teams", force: :cascade do |t|
    t.string   "name"
    t.float    "score",      default: 0.0, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "username"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

  add_foreign_key "answers", "marathons"
  add_foreign_key "answers", "questions"
  add_foreign_key "answers", "teams"
  add_foreign_key "marathon_questions", "marathons"
  add_foreign_key "marathon_questions", "questions"
  add_foreign_key "marathon_teams", "marathons"
  add_foreign_key "marathon_teams", "teams"
  add_foreign_key "marathons", "users"
  add_foreign_key "team_member_of_teams", "member_of_teams"
  add_foreign_key "team_member_of_teams", "teams"
end
