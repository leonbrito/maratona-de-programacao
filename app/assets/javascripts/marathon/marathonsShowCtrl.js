(function() {'use strict';
angular.module('maratona')
.controller('MarathonsShowCtrl', function($scope, $state, marathon, Auth, $mdDialog, marathonsFactory) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.marathon = marathon;
    console.log(marathon.user.username);
    var assoc_marathon = null;
    var assoc_question = null;
    var assoc_team = null;
    var iniciar_marathon = null;
    console.log($scope.marathon);

    // ******************************
    // Dialog Teams
    // ******************************

    $scope.openDialogTeam = function($event) {
      $mdDialog.show({
        controller: DialogTeamCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'assets/marathon/_assocTeamDialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true,
        resolve: {
            teamPromise: function(teamsFactory) {
                return teamsFactory.getAll();
            }
        }
      })
    }

  function DialogTeamCtrl ($timeout, $q, $mdDialog, teamsFactory) {
    var self = this;
    self.simulateQuery = false;
    self.isDisabled    = false;

    self.teams = loadAll();
    self.querySearch   = querySearch;
    self.addTeam = addTeam;

    // ******************************
    // Template methods
    // ******************************

    self.cancel = function($event) {
      $mdDialog.cancel();
    };
    self.finish = function($event) {
      $mdDialog.hide();
    };

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
      var results = query ? self.teams.filter( createFilterFor(query) ) : self.teams,
          deferred;

      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }

    function addTeam() {
      if(!self.selectedItem || self.selectedItem === ''){ return; }
      marathonsFactory.addTeam({
        marathon_id: marathon.id,
        team_id: self.selectedItem.id
      });
      marathon.teams.push(self.selectedItem);
      self.selectedItem = '';
    }

    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
      var repos = teamsFactory.teams;
      return repos.map( function (repo) {
        repo.value = repo.name.toLowerCase();
        return repo;
      });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(item) {
        return (item.value.indexOf(lowercaseQuery) === 0);
      };

    }
  }

  $scope.openDialogRemoveTeam = function($event, marathon, team) {
      assoc_marathon = marathon;
      assoc_team = team;
      $mdDialog.show({
        controller: DialogRemoveTeamCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'assets/marathon/_removeTeamDialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true
      })
    }
    
    function DialogRemoveTeamCtrl ($timeout, $q, $scope, $mdDialog) {
      var self = this;

      // ******************************
      // Template methods
      // ******************************

      self.removeTeam = function($event) {
        marathonsFactory.removeTeam({marathon_id: assoc_marathon.id, team_id: assoc_team.id});
        marathon.teams.pop(assoc_team);
        $mdDialog.cancel();
      };
      self.cancel = function($event) {
        $mdDialog.cancel();
      };
      self.finish = function($event) {
        $mdDialog.hide();
      };
    }

  // ******************************
  // Dialog Questions
  // ******************************

  $scope.openDialogQuestion = function($event) {
    $mdDialog.show({
      controller: DialogQuestionCtrl,
      controllerAs: 'ctrl',
      templateUrl: 'assets/marathon/_assocQuestionDialog.html',
      parent: angular.element(document.body),
      targetEvent: $event,
      clickOutsideToClose:true,
      resolve: {
          questionPromise: function(questionsFactory) {
              return questionsFactory.getAll();
          }
      }
    })
  }

  function DialogQuestionCtrl ($timeout, $q, $mdDialog, questionsFactory) {
    var self = this;
    self.simulateQuery = false;
    self.isDisabled    = false;

    self.questions = loadAll();
    self.querySearch   = querySearch;
    self.addQuestion = addQuestion;

    // ******************************
    // Template methods
    // ******************************

    self.cancel = function($event) {
      $mdDialog.cancel();
    };
    self.finish = function($event) {
      $mdDialog.hide();
    };

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
      var results = query ? self.questions.filter( createFilterFor(query) ) : self.questions,
          deferred;

      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }

    function addQuestion() {
      if(!self.selectedItem || self.selectedItem === ''){ return; }
      marathonsFactory.addQuestion({
        marathon_id: marathon.id,
        question_id: self.selectedItem.id
      });
      marathon.questions.push(self.selectedItem);
      self.selectedItem = '';
    }

    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
      var repos = questionsFactory.questions;
      return repos.map( function (repo) {
        repo.value = repo.name.toLowerCase();
        return repo;
      });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(item) {
        return (item.value.indexOf(lowercaseQuery) === 0);
      };

    }
  }


  $scope.openDialogRemoveQuestion = function($event, marathon, question) {
      assoc_marathon = marathon;
      assoc_question = question;
      $mdDialog.show({
        controller: DialogRemoveQuestionCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'assets/marathon/_removeQuestionDialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true
      })
    }
    
    function DialogRemoveQuestionCtrl ($timeout, $q, $scope, $mdDialog) {
      var self = this;

      // ******************************
      // Template methods
      // ******************************

      self.removeQuestion = function($event) {
        marathonsFactory.removeQuestion({marathon_id: assoc_marathon.id, question_id: assoc_question.id});
        marathon.questions.pop(assoc_question);
        $mdDialog.cancel();
      };
      self.cancel = function($event) {
        $mdDialog.cancel();
      };
      self.finish = function($event) {
        $mdDialog.hide();
      };
    }


    $scope.openDialogIniciar = function($event, marathon) {
      iniciar_marathon = marathon;

      $mdDialog.show({
        controller: DialogIniciarCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'assets/marathon/_inicarDialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true
      })
    }
    
    function DialogIniciarCtrl ($timeout, $q, $scope, $mdDialog, $state) {
      var self = this;

      // ******************************
      // Template methods
      // ******************************

      self.navigateToPath = function(){
        return '#!acompanhamento/' + iniciar_marathon.id;
      }

      self.iniciarMarathon = function($event) {
        marathonsFactory.iniciarMarathon(iniciar_marathon);
        $mdDialog.cancel();
      };
      self.cancel = function($event) {
        $mdDialog.cancel();
      };
      self.finish = function($event) {
        $mdDialog.hide();
      };
    }

})
})();