(function() {'use strict';
angular.module('maratona')
.controller('QuestionsCtrl', function($scope, questionsFactory, Auth, $mdDialog, $mdToast) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.questions = questionsFactory.questions;
    var questionDel = null;

    $scope.addQuestion = function() {
        if(!$scope.name || $scope.name === ''){ return; }
        console.log($scope.name);
        questionsFactory.create({
            name: $scope.name,
        });
        $scope.name = '';
        $scope.showSimpleToast();
    };

    $scope.showSimpleToast = function() {

      $mdToast.show(
        $mdToast.simple()
          .textContent('Sucesso!')
          .position('top right')
          .hideDelay(3000)
          .theme('success-toast')
      );
    };

    $scope.openDialog = function($event, question) {
      questionDel = question;
      $mdDialog.show({
        controller: DialogCtrl,
        controllerAs: 'ctrl',
        templateUrl: 'assets/question/_deleteDialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose:true
      })
    }

    function DialogCtrl ($timeout, $q, $scope, $mdDialog) {
      var self = this;
      console.log(questionDel);

      // ******************************
      // Template methods
      // ******************************

      self.delete = function($event) {
        questionsFactory.delete(questionDel);
        $mdDialog.cancel();
      };
      self.cancel = function($event) {
        $mdDialog.cancel();
      };
      self.finish = function($event) {
        $mdDialog.hide();
      };
    }
})
})();