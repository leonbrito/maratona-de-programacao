json.extract! team_member_of_team, :id, :team_id, :member_of_team_id, :created_at, :updated_at
json.url team_member_of_team_url(team_member_of_team, format: :json)
