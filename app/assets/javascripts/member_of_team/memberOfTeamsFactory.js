(function() {'use strict';
angular.module('maratona')
.factory('memberOfTeamsFactory', function($http, $state, UrlConstant) {

    var o = {
        memberOfTeams: []
    };
    
    o.getAll = function() {
        return $http.get(UrlConstant.member_of_teams).then(function(res){
            angular.copy(res.data, o.memberOfTeams);
        });
    };

    o.get = function(id) {
        return $http.get(UrlConstant.member_of_team + id + '.json').then(function(res) {
            return res.data;
        });
    };

    o.create = function(memberOfTeam) {
        return $http.post(UrlConstant.member_of_teams, memberOfTeam).then(function(data) {
            o.memberOfTeams.push(data.data);
        });
    };

    o.update = function(memberOfTeam) {
        return $http.put('/member_of_teams/' + memberOfTeam.id + '.json', memberOfTeam).then(function(data) {
            $state.go('member_of_teams');
        });
    };

    o.delete = function(memberOfTeam) {
        return $http.delete('/member_of_teams/' + memberOfTeam.id + '.json', memberOfTeam).then(function(res){
            for (var i = 0; i < o.memberOfTeams.length; i++) {
                if(o.memberOfTeams[i].id == memberOfTeam.id){
                    o.memberOfTeams.splice( i, 1 )
                    break;
                }
            }
        });
    };

    return o;
})
})();