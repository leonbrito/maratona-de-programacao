require 'test_helper'

class TeamMemberOfTeamsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @team_member_of_team = team_member_of_teams(:one)
  end

  test "should get index" do
    get team_member_of_teams_url
    assert_response :success
  end

  test "should get new" do
    get new_team_member_of_team_url
    assert_response :success
  end

  test "should create team_member_of_team" do
    assert_difference('TeamMemberOfTeam.count') do
      post team_member_of_teams_url, params: { team_member_of_team: { member_of_team_id: @team_member_of_team.member_of_team_id, team_id: @team_member_of_team.team_id } }
    end

    assert_redirected_to team_member_of_team_url(TeamMemberOfTeam.last)
  end

  test "should show team_member_of_team" do
    get team_member_of_team_url(@team_member_of_team)
    assert_response :success
  end

  test "should get edit" do
    get edit_team_member_of_team_url(@team_member_of_team)
    assert_response :success
  end

  test "should update team_member_of_team" do
    patch team_member_of_team_url(@team_member_of_team), params: { team_member_of_team: { member_of_team_id: @team_member_of_team.member_of_team_id, team_id: @team_member_of_team.team_id } }
    assert_redirected_to team_member_of_team_url(@team_member_of_team)
  end

  test "should destroy team_member_of_team" do
    assert_difference('TeamMemberOfTeam.count', -1) do
      delete team_member_of_team_url(@team_member_of_team)
    end

    assert_redirected_to team_member_of_teams_url
  end
end
