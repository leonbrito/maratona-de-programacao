require 'test_helper'

class MarathonTeamsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @marathon_team = marathon_teams(:one)
  end

  test "should get index" do
    get marathon_teams_url
    assert_response :success
  end

  test "should get new" do
    get new_marathon_team_url
    assert_response :success
  end

  test "should create marathon_team" do
    assert_difference('MarathonTeam.count') do
      post marathon_teams_url, params: { marathon_team: { marathon_id: @marathon_team.marathon_id, team_id: @marathon_team.team_id } }
    end

    assert_redirected_to marathon_team_url(MarathonTeam.last)
  end

  test "should show marathon_team" do
    get marathon_team_url(@marathon_team)
    assert_response :success
  end

  test "should get edit" do
    get edit_marathon_team_url(@marathon_team)
    assert_response :success
  end

  test "should update marathon_team" do
    patch marathon_team_url(@marathon_team), params: { marathon_team: { marathon_id: @marathon_team.marathon_id, team_id: @marathon_team.team_id } }
    assert_redirected_to marathon_team_url(@marathon_team)
  end

  test "should destroy marathon_team" do
    assert_difference('MarathonTeam.count', -1) do
      delete marathon_team_url(@marathon_team)
    end

    assert_redirected_to marathon_teams_url
  end
end
