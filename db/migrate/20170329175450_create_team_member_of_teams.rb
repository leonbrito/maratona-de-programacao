class CreateTeamMemberOfTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :team_member_of_teams do |t|
      t.references :team, foreign_key: true
      t.references :member_of_team, foreign_key: true

      t.timestamps
    end
  end
end
