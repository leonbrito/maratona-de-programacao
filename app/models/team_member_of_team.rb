class TeamMemberOfTeam < ApplicationRecord
  belongs_to :team
  belongs_to :member_of_team
end
