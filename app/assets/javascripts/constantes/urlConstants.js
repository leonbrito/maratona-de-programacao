(function() {'use strict';
angular.module('maratona')
.constant('UrlConstant', UrlConstant());
function UrlConstant() {
    return {
        teams : '/teams.json',
        team : '/teams/',
        marathons : '/marathons.json',
        marathon : '/marathons/',
        questions : '/questions.json',
        question : '/questions/',
        member_of_teams: '/member_of_teams.json',
        member_of_team: '/member_of_teams/'
    }
}
})();