class CreateMarathonTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :marathon_teams do |t|
      t.references :marathon, foreign_key: true
      t.references :team, foreign_key: true

      t.timestamps
    end
  end
end
