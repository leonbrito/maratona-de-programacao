(function() {'use strict';
angular.module('maratona')
.controller('AuthCtrl', function($scope, $state, Auth) {
    
    $scope.login = function() {
        Auth.login($scope.user).then(function() {
            // $state.go('home');
            window.location.href = '/';
        }, function(errorResponse) {
            console.log('Erro no login!');
        });
    };

    $scope.register = function() {
        Auth.register($scope.user).then(function() {
            $state.go('home');
        }, function(errorResponse) {
            console.log('Erro no registro!');
        });
    };
})
})();