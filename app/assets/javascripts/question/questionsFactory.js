(function() {'use strict';
angular.module('maratona')
.factory('questionsFactory', function($http, $state, UrlConstant) {

    var o = {
        questions: []
    };

    o.getAll = function() {
        return $http.get(UrlConstant.questions).then(function(res){
            angular.copy(res.data, o.questions);
        });
    };

    o.get = function(id) {
        return $http.get(UrlConstant.question + id + '.json').then(function(res) {
            return res.data;
        });
    };
    
    o.create = function(question) {
        return $http.post(UrlConstant.questions, question).then(function(data) {
            o.questions.push(data.data);
        });
    };

    o.update = function(question) {
        return $http.put('/questions/' + question.id + '.json', question).then(function(data) {
            $state.go('questions');
        });
    };

    o.delete = function(question) {
        return $http.delete('/questions/' + question.id + '.json', question).then(function(res){
            for (var i = 0; i < o.questions.length; i++) {
                if(o.questions[i].id == question.id){
                    o.questions.splice( i, 1 )
                    break;
                }
            }
        });
    };

    return o;
})
})();