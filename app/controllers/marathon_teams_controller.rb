class MarathonTeamsController < ApplicationController
before_action :authenticate_user!

  # GET /marathon_teams.json
  def index
    respond_with MarathonTeam.all
  end

  # GET /marathon_teams/1.json
  def show
    respond_with @marathon_team = MarathonTeam.find(params[:id])
  end

  # GET /marathon_teams/new
  # def new
  #   @marathon_team = MarathonTeam.new
  # end

  # # GET /marathon_teams/1/edit
  # def edit
  # end

  # POST /marathon_teams.json
  def create
    @marathon_team = MarathonTeam.new(marathon_team_params)
    if @marathon_team.save
      # maraQuestions = MarathonQuestion.where(marathon_id: @marathon_team.marathon_id)
      # maraQuestions.each { |q| Answer.create(marathon_id: @marathon_team.marathon_id, question_id: q.question_id, team_id: @marathon_team.team_id)}
      respond_with @marathon_team
    else
      respond_with @marathon_team.errors
    end
  end

  # Acho que não vai ser necessario
  # PATCH/PUT /marathon_teams/1.json
  # def update
  #   @marathon_team = MarathonTeam.find(params[:id])
  #   if @marathon_team.update(marathon_team_params)
  #     respond_with @marathon_team
  #   else
  #     respond_with @marathon_team.errors
  #   end
  # end

  def removeTeams
    @marathon_team = MarathonTeam.find_by(marathon_id: params[:marathon_id], team_id: params[:team_id])
    @marathon_team.destroy
  end

  # DELETE /marathon_teams/1.json
  def destroy
    respond_with MarathonTeam.find(params[:id]).destroy
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def marathon_team_params
      params.require(:marathon_team).permit(:marathon_id, :team_id)
    end
end