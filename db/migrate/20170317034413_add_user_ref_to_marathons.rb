class AddUserRefToMarathons < ActiveRecord::Migration[5.0]
  def change
    add_reference :marathons, :user, foreign_key: true
  end
end
