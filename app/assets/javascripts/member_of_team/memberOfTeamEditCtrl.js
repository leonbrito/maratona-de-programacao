(function() {'use strict';
angular.module('maratona')
.controller('MemberOfTeamsEditCtrl', function($scope, memberOfTeamsFactory, memberOfTeam, Auth) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.memberOfTeam = memberOfTeam;
    
    $scope.editMemberOfTeam = function(memberOfTeam) {
        if(!$scope.memberOfTeam.name || $scope.memberOfTeam.name === ''){ return; }
        console.log($scope.memberOfTeam);
        memberOfTeamsFactory.update($scope.memberOfTeam);
    };
})
})();