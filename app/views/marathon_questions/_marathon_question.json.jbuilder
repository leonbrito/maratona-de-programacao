json.extract! marathon_question, :id, :marathon_id, :question_id, :created_at, :updated_at
json.url marathon_question_url(marathon_question, format: :json)
