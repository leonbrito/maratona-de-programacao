(function() {'use strict';
angular.module('maratona')
.controller('MarathonsEditCtrl', function($scope, marathonsFactory, marathon, Auth) {
    $scope.singnedIn = Auth.isAuthenticated;
    $scope.marathon = marathon;
    
    $scope.editMarathon = function(marathon) {
        if(!$scope.marathon.duration || $scope.marathon.duration === ''){ return; }
        var d = "" + $scope.marathon.duration;
        $scope.marathon.duration = d.replace("GMT-0200", "GMT-0300");
        console.log($scope.marathon);
        console.log($scope.marathon.duration);
        marathonsFactory.update($scope.marathon);
    };
})
})();