(function() {'use strict';
angular.module('maratona')
.config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $mdAriaProvider, $qProvider, $mdThemingProvider) {
    // Desabilitar warns do ngAria
    $mdAriaProvider.disableWarnings();
    // Remover rejeição do servidor na hora de pegar o usuário atual sem ter alguém logado
    $qProvider.errorOnUnhandledRejections(false);
    // Hearder necessario para realizar requisição post

    let csrfToken = $('meta[name=csrf-token]').attr('content');
    
    $httpProvider.defaults.headers.common.Accept = "application/json, text/plain, */*";
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = csrfToken;
    $httpProvider.defaults.headers.post['X-CSRF-Token'] = csrfToken;
    $httpProvider.defaults.headers.put['X-CSRF-Token'] = csrfToken;
    $httpProvider.defaults.headers.patch['X-CSRF-Token'] = csrfToken;

    // Retirar o !# das urls -- Da problema no refresh
    // $locationProvider.html5Mode({
    //     enabled: true,
    //     requireBase: false
    // });

    $stateProvider
    .state('home', {
        url: '/home',
        templateUrl: 'home/_home.html',
        controller: 'HomeCtrl',
        resolve: {
            marathonPromise: function(marathonsFactory) {
                return marathonsFactory.getAll();
            }
        }
    })
    .state('marathons', {
        url: '/marathons',
        templateUrl: 'marathon/_marathons.html',
        controller: 'MarathonsCtrl',
        resolve: {
            marathons: function(marathonsFactory) {
                return marathonsFactory.getAll();
            }
        }
    })
    .state('marathons/show/:id', {
        url: '/marathons/show/:id',
        templateUrl: 'marathon/_marathons.show.html',
        controller: 'MarathonsShowCtrl',
        resolve: {
            marathon: function($stateParams, marathonsFactory) {
                return marathonsFactory.get($stateParams.id);
            }
        }
    })
    .state('marathons/edit/:id', {
        url: '/marathons/edit/:id',
        templateUrl: 'marathon/_edit.marathons.html',
        controller: 'MarathonsEditCtrl',
        resolve: {
            marathon: function($stateParams, marathonsFactory) {
                return marathonsFactory.get($stateParams.id);
            }
        }
    })
    .state('questions', {
        url: '/questions',
        templateUrl: 'question/_questions.html',
        controller: 'QuestionsCtrl',
        resolve: {
            questionPromise: function(questionsFactory) {
                return questionsFactory.getAll();
            }
        }
    })
    .state('questions/show/:id', {
        url: '/questions/show/:id',
        templateUrl: 'question/_questions.show.html',
        controller: 'QuestionsShowCtrl',
        resolve: {
            question: function($stateParams, questionsFactory) {
                return questionsFactory.get($stateParams.id);
            }
        }
    })
    .state('questions/edit/:id', {
        url: '/questions/edit/:id',
        templateUrl: 'question/_edit.questions.html',
        controller: 'QuestionsEditCtrl',
        resolve: {
            question: function($stateParams, questionsFactory) {
                return questionsFactory.get($stateParams.id);
            }
        }
    })
    .state('teams', {
        url: '/teams',
        templateUrl: 'team/_teams.html',
        controller: 'TeamsCtrl',
        resolve: {
            teamPromise: function(teamsFactory) {
                return teamsFactory.getAll();
            }
        }
    })
    .state('teams/show/:id', {
        url: '/teams/show/:id',
        templateUrl: 'team/_teams.show.html',
        controller: 'TeamsShowCtrl',
        resolve: {
            team: function($stateParams, teamsFactory) {
                return teamsFactory.get($stateParams.id);
            }
        }
    })
    .state('teams/edit/:id', {
        url: '/teams/edit/:id',
        templateUrl: 'team/_edit.teams.html',
        controller: 'TeamsEditCtrl',
        resolve: {
            team: function($stateParams, teamsFactory) {
                return teamsFactory.get($stateParams.id);
            }
        }
    })
    .state('member_of_teams', {
        url: '/member_of_teams',
        templateUrl: 'member_of_team/_member_of_teams.html',
        controller: 'MemberOfTeamsCtrl',
        resolve: {
            memberOfTeamPromise: function(memberOfTeamsFactory) {
                return memberOfTeamsFactory.getAll();
            }
        }
    })
    .state('member_of_teams/show/:id', {
        url: '/member_of_teams/show/:id',
        templateUrl: 'member_of_team/_member_of_teams.show.html',
        controller: 'MemberOfTeamsShowCtrl',
        resolve: {
            memberOfTeam: function($stateParams, memberOfTeamsFactory) {
                return memberOfTeamsFactory.get($stateParams.id);
            }
        }
    })
    .state('member_of_teams/edit/:id', {
        url: '/member_of_teams/edit/:id',
        templateUrl: 'member_of_team/_edit.member_of_teams.html',
        controller: 'MemberOfTeamsEditCtrl',
        resolve: {
            memberOfTeam: function($stateParams, memberOfTeamsFactory) {
                return memberOfTeamsFactory.get($stateParams.id);
            }
        }
    })
    .state('acompanhamento/:id', {
        url: '/acompanhamento/:id',
        templateUrl: 'acompanhamento/_acompanhamento.html',
        controller: 'AcompanhamentoCtrl',
        resolve: {
            marathon: function($stateParams, marathonsFactory) {
                return marathonsFactory.get($stateParams.id);
            }
        }
    })
    .state('login', {
        url: '/login',
        templateUrl: 'auth/_login.html',
        controller: 'AuthCtrl',
        onEnter: function(Auth, $state) {
            Auth.currentUser().then(function (){
                $state.go('home');
            });
        }
    })
    .state('register', {
        url: '/register',
        templateUrl: 'auth/_register.html',
        controller: 'AuthCtrl',
        onEnter: function(Auth, $state) {
            Auth.currentUser().then(function (){
                $state.go('home');
            });
        }
    });

    $urlRouterProvider.otherwise('home');
 })
})();