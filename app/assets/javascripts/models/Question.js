class Question {

    constructor({id, name, created_at, updated_at}) {
        this._id = id;
        this._name = name;
        this._created_at = created_at;
        this._updated_at = updated_at;
    }

    get id() {
        return this._id;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }   

    get created_at() {
        return this._created_at;
    }

    get updated_at() {
        return this._updated_at;
    }
}