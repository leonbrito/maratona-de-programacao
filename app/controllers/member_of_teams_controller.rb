class MemberOfTeamsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]

  # GET /member_of_teams.json
  def index
     respond_with MemberOfTeam.all
  end

  # GET /member_of_teams/1.json
  def show
    respond_with MemberOfTeam.find(params[:id])
  end

  # GET /member_of_teams/new
  # def new
  #   @member_of_team = MemberOfTeam.new
  # end

  # # GET /member_of_teams/1/edit
  # def edit
  # end

  # POST /member_of_teams.json
  def create
    @member_of_team = MemberOfTeam.new(member_of_team_params)
    if @member_of_team.save
        respond_with @member_of_team
    else
        respond_with @member_of_team.errors
    end
  end

  # PATCH/PUT /member_of_teams/1.json
  def update
    @member_of_team = MemberOfTeam.find(params[:id])
    @member_of_team.name = params[:name]
    if @member_of_team.save!
        respond_with @member_of_team
    else
       respond_with @member_of_team.errors
    end
  end

  # DELETE /member_of_teams/1.json
  def destroy
    respond_with MemberOfTeam.find(params[:id]).destroy
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def member_of_team_params
      params.require(:member_of_team).permit(:name)
    end
end