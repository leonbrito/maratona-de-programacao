class RemoveDurationFromMarathon < ActiveRecord::Migration[5.0]
  def change
    reversible do |dir|
      change_table :marathons do |t|
        dir.up   { t.change :duration, :time }
        dir.down { t.change :duration, :string }
      end
    end
  end
end
