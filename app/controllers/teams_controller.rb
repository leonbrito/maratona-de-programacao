class TeamsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  
  # GET /teams.json
  def index
     respond_with Team.all
  end

  # GET /teams/1.json
  def show
    respond_with Team.find(params[:id])
  end

  # GET /teams/new
  # def new
  #   @team = Team.new
  # end

  # GET /teams/1/edit
  # def edit
  # end

  # POST /teams.json
  def create
    @team = Team.new(team_params)
    if @team.save
      respond_with @team
    else
      respond_with @team.errors
    end
  end

  # PATCH/PUT /teams/1.json
  def update
    @team = Team.find(params[:id])
    @team.name = params[:name]
    if @team.save!
      respond_with @team
    else
      respond_with @team.errors
    end
  end

  # DELETE /teams/1.json
  def destroy
    respond_with Team.find(params[:id]).destroy
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def team_params
    params.require(:team).permit(:name, :score)
  end
end
