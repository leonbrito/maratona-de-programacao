require 'test_helper'

class MarathonQuestionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @marathon_question = marathon_questions(:one)
  end

  test "should get index" do
    get marathon_questions_url
    assert_response :success
  end

  test "should get new" do
    get new_marathon_question_url
    assert_response :success
  end

  test "should create marathon_question" do
    assert_difference('MarathonQuestion.count') do
      post marathon_questions_url, params: { marathon_question: { marathon_id: @marathon_question.marathon_id, question_id: @marathon_question.question_id } }
    end

    assert_redirected_to marathon_question_url(MarathonQuestion.last)
  end

  test "should show marathon_question" do
    get marathon_question_url(@marathon_question)
    assert_response :success
  end

  test "should get edit" do
    get edit_marathon_question_url(@marathon_question)
    assert_response :success
  end

  test "should update marathon_question" do
    patch marathon_question_url(@marathon_question), params: { marathon_question: { marathon_id: @marathon_question.marathon_id, question_id: @marathon_question.question_id } }
    assert_redirected_to marathon_question_url(@marathon_question)
  end

  test "should destroy marathon_question" do
    assert_difference('MarathonQuestion.count', -1) do
      delete marathon_question_url(@marathon_question)
    end

    assert_redirected_to marathon_questions_url
  end
end
