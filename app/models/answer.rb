class Answer < ApplicationRecord
  enum status: {default: 0, wrong: 1, certain: 2}
  belongs_to :marathon
  belongs_to :question
  belongs_to :team
end
