class CreateMarathonQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :marathon_questions do |t|
      t.references :marathon, foreign_key: true
      t.references :question, foreign_key: true

      t.timestamps
    end
  end
end
