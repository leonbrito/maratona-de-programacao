(function() {'use strict';
angular.module('maratona')
.factory('acompanhamentoFactory', function($http, $state) {
    var o = {};

    o.submeterCreate = function(answer) {
        return $http.post('/answers.json', answer).then(function(data) {
            return data.data;
        });
    };

    o.submeterUpdate = function(answer) {
        return $http.put('/answers/' + answer.id + '.json', answer).then(function(data) {
        });
    };

    return o;
})
})();